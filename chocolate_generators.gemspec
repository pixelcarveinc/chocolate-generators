$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "chocolate_generators/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "chocolate_generators"
  s.version     = ChocolateGenerators::VERSION
  s.authors     = ["Slava"]
  s.email       = ["slava@pixelcarve.com"]
  s.homepage    = "http://www.pixelcarve.com"
  s.summary     = "Page generators for Chocolate."
  s.description = "Page generators for Chocolate."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.0.5"

  s.add_development_dependency "sqlite3"
end
