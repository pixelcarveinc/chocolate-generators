class Create<%= page_name %>Fields < ActiveRecord::Migration
  def change
    

<%- attributes.each do |field| %>

  <%- if field.split(':').last == 'image' %>
    add_attachment :frame_pages, :<%= field.split(':').first %>
  <%- else %>
    add_column :frame_pages, :<%= field.split(':').first %>, :<%= field.split(':').last %>
  <%- end %>
<%- end %>
  end
end