module Frame
  class <%= page_name_pluralized %>Controller < ApplicationController
    before_action :set_<%= underscored_name %>, only: [:show]
    include FrontendRenderable
    load_and_authorize_resource except: :show, class: Frame::<%= page_name %>
    add_breadcrumb 'pages', :links_path
    add_breadcrumb 'edit', :edit_<%= underscored_name %>_path, only: :edit
    decorates_assigned :<%= underscored_name %>




    # GET /frame/<%= tabelized_name %>
    # GET /frame/<%= tabelized_name %>.json
    def index
      @<%= tabelized_name %> = Frame::<%= page_name %>.all
    end

    # GET /frame/<%= tabelized_name %>/1
    # GET /frame/<%= tabelized_name %>/1.json
    def show


    end

    # GET /frame/<%= tabelized_name %>/new
    def new
      @<%= underscored_name %> = Frame::<%= page_name %>.new
    end






    # GET /frame/<%= tabelized_name %>/1/edit
    def edit
    end



    # PATCH/PUT /frame/<%= tabelized_name %>/1
    # PATCH/PUT /frame/<%= tabelized_name %>/1.json
    def update
      respond_to do |format|
        if @<%= underscored_name %>.update(<%= underscored_name %>_params)
          format.html { redirect_to links_path, notice: '<%= page_name %> was successfully updated.' }
          format.json { render :show, status: :ok, location: @<%= underscored_name %> }
        else
          format.html { render :edit }
          format.json { render json: @<%= underscored_name %>.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /frame/<%= tabelized_name %>/1
    # DELETE /frame/<%= tabelized_name %>/1.json
    def destroy
      @<%= underscored_name %>.destroy
      respond_to do |format|
        format.html { redirect_to <%= tabelized_name %>_url }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_<%= underscored_name %>
      
        @<%= underscored_name %> = @page
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def <%= underscored_name %>_params
        params.require(:<%= underscored_name %>).permit(<%= @fields.join(",") %>)
      end
  end
end
