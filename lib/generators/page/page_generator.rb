require 'rails/generators'
require 'rails/generators/migration'
require 'rails/generators/active_record'

class PageGenerator < Rails::Generators::Base#NamedBase
  include Rails::Generators::Migration

  source_root File.expand_path('../templates', __FILE__)

  argument :page_name, type: :string, :required => true, banner:'Page Type name'
  argument :attributes, type: :array, default: [], banner: "field:type field:type"


  def create_controller
    default_fields = [:type, :title, :body, :meta_description, :head_title, :published]

    @fields = (default_fields + attributes.map { |field| field.split(':').first}).map { |field| ':' + field.to_s }


    template "pages_controller.rb", "app/controllers/frame/#{tabelized_name}_controller.rb"
    template "page.rb", "app/models/frame/#{underscored_name}.rb"
    template "page_decorator.rb", "app/decorators/frame/#{underscored_name}_decorator.rb"
    inject_into_file 'config/routes.rb', after: '# be edited in the CMS.' do
      "\n  resources :#{tabelized_name}, only: [:edit, :update]"
    end

    
  end

  def append_application

    inject_into_file 'config/application.rb', after: '# Add custom page types' do
      "\n  config.frame.page_types << 'Frame::#{page_name}'"
    end
  end

  def append_frame_router

    inject_into_file 'app/assets/javascripts/frontend/FrameRouter.js.coffee', after: '# Custom page types below' do
      "\n        when '#{page_name}'\n          m[slug + '(/)'] = '#{underscored_name.camelize(:lower)}Page'"
    end
  end

  def create_javascript_class
    template "PageType.js.coffee", "app/assets/javascripts/frontend/views/pages/#{page_name}.js.coffee"
  end

  def append_page_controller 

    inject_into_file 'app/assets/javascripts/frontend/PageController.js.coffee', after: '# Custom page types below' do
      "\n  #{underscored_name.camelize(:lower)}Page: ->\n    @changePage '#{page_name}'"
    end
  end

  def create_public_activity
 
    empty_directory "app/views/public_activity/frame_#{underscored_name}"

    template "public_activity/_create.haml", "app/views/public_activity/frame_#{underscored_name}/_create.haml"
    template "public_activity/_destroy.haml", "app/views/public_activity/frame_#{underscored_name}/_destroy.haml"
    template "public_activity/_update.haml", "app/views/public_activity/frame_#{underscored_name}/_update.haml"

  end

  def create_views
    empty_directory "app/views/frame/#{tabelized_name}"
    template "view/edit.html.haml", "app/views/frame/#{tabelized_name}/edit.html.haml"
    template "view/show.html.haml", "app/views/frame/#{tabelized_name}/show.html.haml"
  end

  def create_page_migration
    migration_template "migration/create_page_fields.rb", "db/migrate/create_#{underscored_name}_fields.rb"
    rake "db:migrate"
  end

  private

  def tabelized_name
    page_name.tableize
  end
  def underscored_name
    page_name.underscore
  end
  def page_name_pluralized
    page_name.pluralize
  end
  def self.next_migration_number(dir)
    Time.now.utc.strftime("%Y%m%d%H%M%S")
  end
end
