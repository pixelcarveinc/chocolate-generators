require 'active_record'
require 'rails/generators/actions/create_migration'
require 'rails/generators'
require 'rails/generators/migration'
require "rake"

ChocolateCMS::Application.load_tasks

class GalleryGenerator < Rails::Generators::Base#NamedBase
  extend ActiveSupport::Concern
  include Rails::Generators::Migration
  #include ActiveRecord::Generators::Base

  source_root File.expand_path('../templates', __FILE__)

  argument :gallery_name, type: :string, :required => true, banner:'Gallery Type name'
  argument :image_name, type: :string, :required => true, banner:'Image Type name'
  argument :attributes, type: :array, default: [], banner: "gallery_field:type gallery_field:type ^ image_field:type image_field:type"


  def create_gallery_essential_files
    gallery_default_fields = [:type, :title, :body, :meta_description, :head_title, :published]
    image_default_fields = [:type, :title, :body, :meta_description, :head_title, :published, :image, :alt]

    @gallery_attributes = []
    @image_attributes = []



    #View USAGE file for syntax
    if attributes.index('^') == 0 #Image only fields
      @image_attributes = attributes[attributes.index('^')+1..-1]


    elsif attributes.index('^') == nil #Gallery only fields

      @gallery_attributes = attributes


    else #Fields for both types
      @gallery_attributes = attributes[0..attributes.index('^')-1]
      @image_attributes = attributes[attributes.index('^')+1..-1]


    end

    @gallery_fields = (gallery_default_fields + @gallery_attributes.map { |field| field.split(':').first}).map { |field| ':' + field.to_s } if @gallery_attributes
    @image_fields = (image_default_fields + @image_attributes.map { |field| field.split(':').first}).map { |field| ':' + field.to_s } if @image_attributes





    template "gallery/gallery.rb", "app/models/frame/#{gallery_name.underscore}.rb"
    template "gallery/galleries_controller.rb", "app/controllers/frame/#{gallery_name.tableize}_controller.rb"
    template "gallery/gallery_decorator.rb", "app/decorators/frame/#{gallery_name.underscore}_decorator.rb"


  end

  def create_image_essential_files
    #default_fields = [:type, :title, :body, :meta_description, :head_title, :published]

    #@fields = (default_fields + attributes.map { |field| field.split(':').first}).map { |field| ':' + field.to_s }

    template "image/image.rb", "app/models/frame/#{image_name.underscore}.rb"
    template "image/images_controller.rb", "app/controllers/frame/#{image_name.tableize}_controller.rb"
    template "image/image_decorator.rb", "app/decorators/frame/#{image_name.underscore}_decorator.rb"

  end

  def append_config_routes

    inject_into_file 'config/routes.rb', after: '# be edited in the CMS.' do
      "\n  resources :#{gallery_name.tableize}, only: [:edit, :update] do\n    resources :#{image_name.tableize}, except: [:show], concerns: :multidestroyable\n  end"
    end

  end

  def append_application

    inject_into_file 'config/application.rb', after: '# Add custom page types' do
      "\n    config.frame.page_types << 'Frame::#{gallery_name}'"
    end
  end

  def append_frame_router

    inject_into_file 'app/assets/javascripts/frontend/FrameRouter.js.coffee', after: '# Custom page types below' do
      "\n        when '#{gallery_name}'\n          m[slug + '(/)'] = '#{gallery_name.underscore.camelize(:lower)}Page'\n          m[slug + '/:#{image_name.underscore.camelize(:lower)}(/)'] = '#{image_name.underscore.camelize(:lower)}Page'"
    end
  end


  def create_javascript_class
    template "gallery/PageType.js.coffee", "app/assets/javascripts/frontend/views/pages/#{gallery_name}.js.coffee"
    template "image/PageType.js.coffee", "app/assets/javascripts/frontend/views/pages/#{image_name}.js.coffee"
  end

  def append_page_controller

    inject_into_file 'app/assets/javascripts/frontend/PageController.js.coffee', after: '# Custom page types below' do
      "\n  #{gallery_name.underscore.camelize(:lower)}Page: ->\n    @changePage '#{gallery_name}'\n  #{image_name.underscore.camelize(:lower)}Page: ->\n    @changePage '#{image_name}'"
    end
  end

  def create_public_activity

    empty_directory "app/views/public_activity/frame_#{gallery_name.underscore}"

    template "gallery/public_activity/_create.haml", "app/views/public_activity/frame_#{gallery_name.underscore}/_create.haml"
    template "gallery/public_activity/_destroy.haml", "app/views/public_activity/frame_#{gallery_name.underscore}/_destroy.haml"
    template "gallery/public_activity/_update.haml", "app/views/public_activity/frame_#{gallery_name.underscore}/_update.haml"

    empty_directory "app/views/public_activity/frame_#{image_name.underscore}"

    template "image/public_activity/_create.haml", "app/views/public_activity/frame_#{image_name.underscore}/_create.haml"
    template "image/public_activity/_destroy.haml", "app/views/public_activity/frame_#{image_name.underscore}/_destroy.haml"
    template "image/public_activity/_update.haml", "app/views/public_activity/frame_#{image_name.underscore}/_update.haml"

  end


  def create_views
    empty_directory "app/views/frame/#{gallery_name.tableize}"
    template "gallery/view/edit.html.haml", "app/views/frame/#{gallery_name.tableize}/edit.html.haml"
    template "gallery/view/show.html.haml", "app/views/frame/#{gallery_name.tableize}/show.html.haml"

    empty_directory "app/views/frame/#{image_name.tableize}"
    template "image/view/edit.html.haml", "app/views/frame/#{image_name.tableize}/edit.html.haml"
    template "image/view/show.html.haml", "app/views/frame/#{image_name.tableize}/show.html.haml"
    template "image/view/_form.html.haml", "app/views/frame/#{image_name.tableize}/_form.html.haml"
    template "image/view/index.html.haml", "app/views/frame/#{image_name.tableize}/index.html.haml"
    template "image/view/new.html.haml", "app/views/frame/#{image_name.tableize}/new.html.haml"
  end

  def create_routables

    template "gallery/routables/gallery_routable.rb", "app/routables/frame/#{gallery_name.underscore}_routable.rb"
  end

  def create_image_migration
    # if @behavior == :invoke
    #   migration_template "image/migration/create_image_attributes.rb", "db/migrate/create_frame_#{image_name.tableize}.rb"
    #   rake "db:migrate"
    if @behavior != :invoke && self.class.migration_exists?("db/migrate","create_frame_#{image_name.tableize}")

      version = self.class.migration_exists?("db/migrate","create_frame_#{image_name.tableize}").match(/[0-9]+/)

      #pry
      ENV["VERSION"] = version[0]

      Rake::Task["db:migrate:down"].invoke

      #FileUtils.rm("db/migrate/#{version}_create_frame_#{image_name.tableize}.rb")
    end
    #the following will create or delete deppending on @behavior
    migration_template "image/migration/create_image_fields.rb", "db/migrate/create_frame_#{image_name.tableize}.rb"

    #for some reason, the bottom line does not reverse migration, as a solution using db:migrate:down above
    rake "db:migrate"
  end

  def create_gallery_migration
    if @behavior != :invoke && self.class.migration_exists?("db/migrate","create_frame_#{gallery_name.tableize}")

      version = self.class.migration_exists?("db/migrate","create_frame_#{gallery_name.tableize}").match(/[0-9]+/)


      #pry
      ENV["VERSION"] = version[0]
      Rake::Task["db:migrate:down"].reenable
      Rake::Task["db:migrate:down"].invoke

      #FileUtils.rm("db/migrate/#{version}_create_frame_#{image_name.tableize}.rb")
    end


    migration_template "gallery/migration/create_gallery_fields.rb", "db/migrate/create_frame_#{gallery_name.tableize}.rb"

    rake "db:migrate"
  end



  def create_backend_routes
    empty_directory "app/assets/javascripts/backend/helpers"
    empty_directory "app/assets/javascripts/backend/models"
    empty_directory "app/assets/javascripts/backend/views"

    template "image/backend/model/Image.js.coffee", "app/assets/javascripts/backend/models/#{image_name}.js.coffee"
    template "image/backend/helper/paths.js.coffee", "app/assets/javascripts/backend/helpers/#{gallery_name.underscore}_#{image_name.underscore}_paths.js.coffee"

  end


  private
  def self.next_migration_number(dirname) #:nodoc:

    next_num = current_migration_number(dirname) + 1


    ::ActiveRecord::Migration.next_migration_number(next_num)
  end
end
