module Frame
  class <%= gallery_name %>Routable < Routable

    def content_routes(mapper)

      mapper.link.page.<%= image_name.tableize %>.where(published: true).each do |<%= image_name.underscore %>|
        mapper.get <%= image_name.underscore %>.slug, {
          controller: Frame::<%= image_name.pluralize %>Controller,
          defaults: {link_id: mapper.link.id, <%= image_name.underscore %>_id: <%= image_name.underscore %>.id}
        }
      end
    end

  end
end
