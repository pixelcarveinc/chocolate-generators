module Frame
  class <%= gallery_name.pluralize %>Controller < ApplicationController

    include FrontendRenderable
    load_and_authorize_resource except: :show, class: Frame::<%= gallery_name %>
    before_action :set_<%= gallery_name.underscore %>, :set_<%= image_name.underscore %>, only: [:show]
    add_breadcrumb 'pages', :links_path
    add_breadcrumb 'edit', :edit_<%= gallery_name.underscore %>_path, only: :edit
    decorates_assigned :<%= gallery_name.underscore %>

    def index
      @<%= gallery_name.tableize %> = Frame::<%= gallery_name %>.all
    end

    def show

    end

    
    def edit
    end

    def update
      respond_to do |format|
        if @<%= gallery_name.underscore %>.update(<%= gallery_name.underscore %>_params)
          format.html { redirect_to links_path, notice: '<%= gallery_name %> was successfully updated.' }
          format.json { render :show, status: :ok, location: @<%= gallery_name.underscore %> }
        else
          format.html { render :edit }
          format.json { render json: @<%= gallery_name.underscore %>.errors, status: :unprocessable_entity }
        end
      end
    end

    private

      def set_<%= gallery_name.underscore %>
        @<%= gallery_name.underscore %> = @page
      end

      def set_<%= image_name.underscore %>
        @<%= image_name.tableize %> = @<%= gallery_name.underscore %>.<%= image_name.tableize %>.where(published: true).order(:position)
      end

      
      def <%= gallery_name.underscore %>_params
        params.require(:<%= gallery_name.underscore %>).permit(<%= @gallery_fields.join(",") %>)
      end
  end
end
