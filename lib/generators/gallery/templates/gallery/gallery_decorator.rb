module Frame
  class <%= gallery_name %>Decorator < PageDecorator
    delegate_all

    decorates_association :<%= image_name.tableize %>

    def <%= image_name.tableize %>_path(options = {})
      h.<%= gallery_name.underscore %>_<%= image_name.tableize %>_path object, options
    end

    def new_<%= image_name.underscore %>_path(options = {})
      h.new_<%= gallery_name.underscore %>_<%= image_name.underscore %>_path object, options
    end


    # Define presentation-specific methods here. Helpers are accessed through
    # `helpers` (aka `h`). You can override attributes, for example:
    #
    #   def created_at
    #     helpers.content_tag :span, class: 'time' do
    #       object.created_at.strftime("%a %m/%d/%y")
    #     end
    #   end

  end
end
