#= require frame/namespace


class window.chocolate.frame.models.<%= image_name %> extends Backbone.Model

  url: -> JST.frame<%= gallery_name %><%= image_name %>Path(@get('<%= gallery_name.underscore %>_id'), @id)



