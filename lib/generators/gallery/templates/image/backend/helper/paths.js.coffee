
_paths =
  links: ''
  pages: ''

pxcv.addHelpers


  frame<%= gallery_name %>Path: (id) -> @framePagePath(id).replace(/pages/, '<%= gallery_name.tableize %>')

  frame<%= gallery_name %><%= image_name.pluralize %>Path: (id) -> @frame<%= gallery_name %>Path(id) + '/<%= image_name.tableize %>'

  frame<%= gallery_name %><%= image_name %>Path: (galleryId, imageId) ->
    @frame<%= gallery_name %><%= image_name.pluralize %>Path(galleryId) + "/#{imageId}"

  newFrame<%= gallery_name %><%= image_name %>Path: (galleryId, imageId) ->
    @frame<%= gallery_name %><%= image_name %>Path(galleryId, imageId) + '/new'

  editFrame<%= gallery_name %><%= image_name %>Path: (galleryId, imageId) ->
    @frame<%= gallery_name %><%= image_name %>Path(galleryId, imageId) + '/edit'




