module Frame
  class <%= image_name %>Decorator < Draper::Decorator
    delegate_all

    def path(options = {})
        if object.new_record?
          h.<%= gallery_name.underscore %>_<%= image_name.tableize %>_path object.<%= gallery_name.underscore %>, options
        else
          h.<%= gallery_name.underscore %>_<%= image_name.underscore %>_path object.<%= gallery_name.underscore %>, object, options
        end
      end

      def edit_path(options = {})
        h.edit_<%= gallery_name.underscore %>_<%= image_name.underscore %>_path object.<%= gallery_name.underscore %>, object, options
      end

      def row_json
        object.to_json only: [:id, :<%= gallery_name.underscore %>_id, :position, :published]
      end

      def lightbox_thumb
        h.link_to object.image.url, title: object.title, data: { lightbox: 'thumb' } do
          h.image_tag object.image.url(:tiny)
        end
      end

  end
end
