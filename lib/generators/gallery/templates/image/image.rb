module Frame
  class <%= image_name %> < ActiveRecord::Base
    include ::PublicActivity::Model
    include GeneratesRoutes

    before_save :set_alt

    has_attached_file :image,
      styles: {
        tiny:   ['40x40#',     :jpg],
        small:  ['100x>',      :jpg],
        medium: ['250x250>',   :jpg],
        large:  ['500x500>',   :png],
        huge:   ['1000x1000>', :png],
        slideshow:   ['1920x500#', :jpg],
        thumb: ''},
      convert_options: {
        thumb: '-resize 256x272! -gravity center -crop 190x250+0+0 -quality 80' }

    #validates :image, attachment_presence: true
    validates_attachment_content_type :image, :content_type => /^image\/.*$/

    tracked params: {title: :title}, owner: proc { |c, m| c.current_user if c }

    belongs_to :<%= gallery_name.underscore %>, inverse_of: :<%= image_name.tableize %>
    acts_as_list scope: :<%= gallery_name.underscore %>, top_of_list: 0

    validates_presence_of :<%= gallery_name.underscore %>


    before_save :prepare_slug



<%- @image_attributes.each do |field| %>
  <%- if field.split(':').last == 'image' %>
    has_attached_file :<%= field.split(':').first %>,
            styles: {
              hires:  ['1200x>',      :jpg],
              midres:  ['768x>',      :jpg],
              lores:  ['550x>',      :jpg],
              tiny:   ['40x40#',     :jpg],
              small:  ['100x>',      :jpg],
              medium: ['250x250#',   :jpg],
              large:  ['500x500#',   :jpg],
              huge:   ['1000x1000#', :jpg],
              background: ['1440x916#', :jpg] },
            convert_options: {
              hires:   '-strip -depth 8 -quality 90',
              midres:   '-strip -depth 8 -quality 90',
              lores:   '-strip -depth 8 -quality 90',
              tiny:   '-flatten -strip -depth 8 -quality 90',
              small:  '-flatten -strip -depth 8 -quality 90',
              medium: '-flatten -strip -depth 8 -quality 90',
              large:  '-flatten -strip -depth 8 -quality 90',
              huge:   '-flatten -strip -depth 8 -quality 90',
              background:   '-flatten -strip -depth 8 -quality 90' }

    validates_attachment_content_type :<%= field.split(':').first %>, :content_type => /^image\/.*$/
  <%- end %>
<%- end %>

    private

      def set_title
        if title.blank?
          self.title = ::File.basename image_file_name, ::File.extname(image_file_name)
        end
      end

      def set_alt
        self.alt = '' if alt.blank?
      end

      def prepare_slug
        self.slug = title.create_id if slug.blank?
        self.slug = unique_slug slug
      end

      def unique_slug(slug)
        if <%= gallery_name.underscore %>.<%= image_name.tableize %>.any? { |p| p != self && p.slug == slug }
          # duplicate slug, need to make it unique
          # if -\d+$ matches increment end number, otherwise add a -1
          if slug =~ /-(\d+)$/
            slug.sub!(/\d+$/, ($~[1].to_i + 1).to_s)
          else
            slug = slug + '-1'
          end

          # try again
          return unique_slug slug
        end
        slug
      end
  end
end
