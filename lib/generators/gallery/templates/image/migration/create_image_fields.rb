class CreateFrame<%= image_name.pluralize %> < ActiveRecord::Migration
  def change
    create_table :frame_<%= image_name.tableize %> do |t|
      t.integer :<%= gallery_name.underscore %>_id
      t.string :title
      t.string :slug
      t.string :alt
      t.text :body
      t.boolean :published, default: false
      t.integer :position







      t.timestamps
    end
    
<%- @image_attributes.each do |field| %>

  <%- if field.split(':').last == 'image' %>
    add_attachment :frame_<%= image_name.tableize %>, :<%= field.split(':').first %>
  <%- else %>
    add_column :frame_<%= image_name.tableize %>, :<%= field.split(':').first %>, :<%= field.split(':').last %>
  <%- end %>
<%- end %>

    add_attachment :frame_<%= image_name.tableize %>, :image
    add_index :frame_<%= image_name.tableize %>, [:<%= gallery_name.underscore %>_id, :position]
  end
end



#TODO: add all additional attributes to migration, controller and model. ---also check if image attribute is being added to GALLERY model. 