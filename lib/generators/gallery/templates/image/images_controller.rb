module Frame
  class <%= image_name.pluralize %>Controller < ApplicationController

    include FrontendRenderable

    before_action :set_<%= gallery_name.underscore %>, except: :show
    before_action :set_<%= image_name.underscore %>, only: :show

    load_and_authorize_resource except: [:show, :create, :multi_destroy], through: :<%= gallery_name.underscore %>, class: Frame::<%= image_name %>

    decorates_assigned :<%= gallery_name.underscore %>, :<%= image_name.underscore %>, :<%= image_name.tableize %>


    # GET /frame/<%= image_name.tableize %>
    # GET /frame/<%= image_name.tableize %>.json
    def index

    end

    # GET /frame/<%= image_name.tableize %>/1
    # GET /frame/<%= image_name.tableize %>/1.json
    def show
    end

    # GET /frame/<%= image_name.tableize %>/new
    def new
      add_breadcrumb 'new', :new_<%= gallery_name.underscore %>_<%= image_name.underscore %>_path
      #@<%= image_name.underscore %> = Frame::<%= image_name %>.new
    end

    # GET /frame/<%= image_name.tableize %>/1/edit
    def edit
      add_breadcrumb 'edit', :edit_<%= gallery_name.underscore %>_<%= image_name.underscore %>_path
    end

    # POST /frame/<%= image_name.tableize %>
    # POST /frame/<%= image_name.tableize %>.json
    def create
      @<%= image_name.underscore %> = @<%= gallery_name.underscore %>.<%= image_name.tableize %>.new(<%= image_name.underscore %>_params)
      authorize! :create, @<%= image_name.underscore %>

      respond_to do |format|

        if @<%= image_name.underscore %>.save
          format.html { redirect_to <%= gallery_name.underscore %>_<%= image_name.tableize %>_path(@<%= gallery_name.underscore %>), notice: '<%= image_name %> was successfully created.' }
          format.json { render :show, status: :created, location: [@<%= gallery_name.underscore %>, @<%= image_name.underscore %>] }
        else

          format.html { render action: 'new', notice: 'errors: ' + @<%= image_name.underscore %>.errors.inspect }
          format.json { render json: @<%= image_name.underscore %>.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /frame/<%= image_name.tableize %>/1
    # PATCH/PUT /frame/<%= image_name.tableize %>/1.json
    def update
      respond_to do |format|
        if @<%= image_name.underscore %>.update(<%= image_name.underscore %>_params)
          format.html { redirect_to <%= gallery_name.underscore %>_<%= image_name.tableize %>_path(@<%= gallery_name.underscore %>), notice: '<%= image_name %> was successfully updated.' }
          format.json { render json: @<%= image_name.underscore %>, status: :ok, location: [@<%= gallery_name.underscore %>, @<%= image_name.underscore %>] }
        else
          format.html { render :edit, notice: 'errors: ' + @<%= image_name.underscore %>.errors }
          format.json { render json: @<%= image_name.underscore %>.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /frame/<%= image_name.tableize %>/1
    # DELETE /frame/<%= image_name.tableize %>/1.json
    def destroy
      @<%= image_name.underscore %>.destroy
      respond_to do |format|
        format.html { redirect_to <%= gallery_name.underscore %>_<%= image_name.tableize %>_url(@<%= gallery_name.underscore %>), notice: '<%= image_name %> was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
    # DELETE /blogs/1/posts
    def multi_destroy
      @<%= image_name.tableize %> = Frame::<%= image_name %>.find params[:id]
      authorize! :destroy, @<%= image_name.tableize %>

      <%= image_name %>.destroy @<%= image_name.tableize %>

      respond_to do |format|
        format.html { redirect_to <%= gallery_name.underscore %>_<%= image_name.tableize %>_url(@<%= gallery_name.underscore %>) }
        format.json { head :no_content }
      end
    end

    private






      # Use callbacks to share common setup or constraints between actions.
      def set_<%= gallery_name.underscore %>
        @<%= gallery_name.underscore %> = Frame::<%= gallery_name %>.find(params[:<%= gallery_name.underscore %>_id])
        add_breadcrumb 'pages', :links_path
        add_breadcrumb @<%= gallery_name.underscore %>.title, edit_page_path(@<%= gallery_name.underscore %>)
        add_breadcrumb '<%= image_name.tableize %>', :<%= gallery_name.underscore %>_<%= image_name.tableize %>_path
      end

      def set_<%= image_name.underscore %>

        @<%= gallery_name.underscore %> = @page
        @<%= image_name.underscore %> = @<%= gallery_name.underscore %>.<%= image_name.tableize %>.find(params[:<%= image_name.underscore %>_id])
        @<%= image_name.tableize %> = @<%= gallery_name.underscore %>.<%= image_name.tableize %>.where(<%= gallery_name.underscore %>_id: @<%= gallery_name.underscore %>.id)
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def <%= image_name.underscore %>_params
        params.require(:<%= image_name.underscore %>).permit(<%= @image_fields.join(",") %>)
      end
  end
end
