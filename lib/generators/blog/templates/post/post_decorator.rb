module Frame
  class <%= post_name %>Decorator < Draper::Decorator
    delegate_all

    def path(options = {})
      if object.new_record?
        h.<%= blog_name.underscore %>_<%= post_name.tableize %>_path object.<%= blog_name.underscore %>, options
      else
        h.<%= blog_name.underscore %>_<%= post_name.underscore %>_path object.<%= blog_name.underscore %>, object, options
      end
    end

    def edit_path(options = {})
      h.edit_<%= blog_name.underscore %>_<%= post_name.underscore %>_path object.<%= blog_name.underscore %>, object, options
    end
    
    def row_json
      object.to_json only: [:id, :<%= blog_name.underscore %>_id, :published]
    end
    
    def body_short
      if object.body && object.body != ""
        truncate html_sanitizer.sanitize(object.body)
      else
        object.body
      end
    end
    
    private
    
      def html_sanitizer
        @html_sanitizer ||= HTML::FullSanitizer.new
      end
      
      # TODO extend String with this?
      def truncate(str, options = {})
        options = {words: 12}.merge options
        w = str.split(/\s/)
        n = options[:words]
        w[0...n].join(' ') + (w.size > n ? '...' : '')
      end

  end
end


