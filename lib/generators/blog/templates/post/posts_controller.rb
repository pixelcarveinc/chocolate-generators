module Frame
  class <%= post_name.pluralize %>Controller < ApplicationController

    include FrontendRenderable

    before_action :set_<%= blog_name.underscore %>, except: :show
    before_action :set_<%= post_name.underscore %>, only: :show

    load_and_authorize_resource except: [:show, :create, :multi_destroy], through: :<%= blog_name.underscore %>, class: Frame::<%= post_name %>

    decorates_assigned :<%= blog_name.underscore %>, :<%= post_name.underscore %>, :<%= post_name.tableize %>


    # GET /frame/<%= post_name.tableize %>
    # GET /frame/<%= post_name.tableize %>.json
    def index

    end

    # GET /frame/<%= post_name.tableize %>/1
    # GET /frame/<%= post_name.tableize %>/1.json
    def show
    end

    # GET /frame/<%= post_name.tableize %>/new
    def new
      add_breadcrumb 'new', :new_<%= blog_name.underscore %>_<%= post_name.underscore %>_path
      #@<%= post_name.underscore %> = Frame::<%= post_name %>.new
    end

    # GET /frame/<%= post_name.tableize %>/1/edit
    def edit
      add_breadcrumb 'edit', :edit_<%= blog_name.underscore %>_<%= post_name.underscore %>_path
    end

    # POST /frame/<%= post_name.tableize %>
    # POST /frame/<%= post_name.tableize %>.json
    def create
      @<%= post_name.underscore %> = @<%= blog_name.underscore %>.<%= post_name.tableize %>.new(<%= post_name.underscore %>_params)
      authorize! :create, @<%= post_name.underscore %>

      respond_to do |format|

        if @<%= post_name.underscore %>.save
          format.html { redirect_to <%= blog_name.underscore %>_<%= post_name.tableize %>_path(@<%= blog_name.underscore %>), notice: '<%= post_name %> was successfully created.' }
          format.json { render :show, status: :created, location: [@<%= blog_name.underscore %>, @<%= post_name.underscore %>] }
        else

          format.html { render action: 'new', notice: 'errors: ' + @<%= post_name.underscore %>.errors.inspect }
          format.json { render json: @<%= post_name.underscore %>.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /frame/<%= post_name.tableize %>/1
    # PATCH/PUT /frame/<%= post_name.tableize %>/1.json
    def update
      respond_to do |format|
        if @<%= post_name.underscore %>.update(<%= post_name.underscore %>_params)
          format.html { redirect_to <%= blog_name.underscore %>_<%= post_name.tableize %>_path(@<%= blog_name.underscore %>), notice: '<%= post_name %> was successfully updated.' }
          format.json { render json: @<%= post_name.underscore %>, status: :ok, location: [@<%= blog_name.underscore %>, @<%= post_name.underscore %>] }
        else
          format.html { render :edit, notice: 'errors: ' + @<%= post_name.underscore %>.errors }
          format.json { render json: @<%= post_name.underscore %>.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /frame/<%= post_name.tableize %>/1
    # DELETE /frame/<%= post_name.tableize %>/1.json
    def destroy
      @<%= post_name.underscore %>.destroy
      respond_to do |format|
        format.html { redirect_to <%= blog_name.underscore %>_<%= post_name.tableize %>_url(@<%= blog_name.underscore %>), notice: '<%= post_name %> was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
    # DELETE /blogs/1/posts
    def multi_destroy
      @<%= post_name.tableize %> = Frame::<%= post_name %>.find params[:id]
      authorize! :destroy, @<%= post_name.tableize %>

      <%= post_name %>.destroy @<%= post_name.tableize %>

      respond_to do |format|
        format.html { redirect_to <%= blog_name.underscore %>_<%= post_name.tableize %>_url(@<%= blog_name.underscore %>) }
        format.json { head :no_content }
      end
    end

    private






      # Use callbacks to share common setup or constraints between actions.
      def set_<%= blog_name.underscore %>
        @<%= blog_name.underscore %> = Frame::<%= blog_name %>.find(params[:<%= blog_name.underscore %>_id])
        add_breadcrumb 'pages', :links_path
        add_breadcrumb @<%= blog_name.underscore %>.title, edit_page_path(@<%= blog_name.underscore %>)
        add_breadcrumb '<%= post_name.tableize %>', :<%= blog_name.underscore %>_<%= post_name.tableize %>_path
      end

      def set_<%= post_name.underscore %>

        @<%= blog_name.underscore %> = @page
        @<%= post_name.underscore %> = @<%= blog_name.underscore %>.<%= post_name.tableize %>.find(params[:<%= post_name.underscore %>_id])
        @<%= post_name.tableize %> = @<%= blog_name.underscore %>.<%= post_name.tableize %>.where(<%= blog_name.underscore %>_id: @<%= blog_name.underscore %>.id)
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def <%= post_name.underscore %>_params
        params.require(:<%= post_name.underscore %>).permit(<%= @post_fields.join(",") %>)
      end
  end
end
