class CreateFrame<%= post_name.pluralize %> < ActiveRecord::Migration
  def change
    create_table :frame_<%= post_name.tableize %> do |t|
      t.integer :<%= blog_name.underscore %>_id
      t.string :category
      t.string :title
      t.string :slug
      t.text :body
      t.boolean :published, default: false
      t.datetime :publish_date







      t.timestamps
    end
    
<%- @post_attributes.each do |field| %>

  <%- if field.split(':').last == 'post' %>
    add_attachment :frame_<%= post_name.tableize %>, :<%= field.split(':').first %>
  <%- else %>
    add_column :frame_<%= post_name.tableize %>, :<%= field.split(':').first %>, :<%= field.split(':').last %>
  <%- end %>
<%- end %>


    add_index :frame_<%= post_name.tableize %>, :<%= blog_name.underscore %>_id
  end
end



#TODO: add all additional attributes to migration, controller and model. ---also check if post attribute is being added to GALLERY model. 