module Frame
  class <%= post_name %> < ActiveRecord::Base
    include ::PublicActivity::Model
    include GeneratesRoutes
    
    tracked params: {title: :title}, owner: proc { |c, m| c.current_user if c }
    
    belongs_to :<%= blog_name.underscore %>, inverse_of: :<%= post_name.tableize %>
    
    validates :title, presence: true
    validates_presence_of :<%= blog_name.underscore %>
    
    after_initialize :set_default_publish_date
    before_save :prepare_slug


<%- @post_attributes.each do |field| %>
  <%- if field.split(':').last == 'post' %>
    has_attached_file :<%= field.split(':').first %>,
            styles: {
              hires:  ['1200x>',      :jpg],
              midres:  ['768x>',      :jpg],
              lores:  ['550x>',      :jpg],
              tiny:   ['40x40#',     :jpg],
              small:  ['100x>',      :jpg],
              medium: ['250x250#',   :jpg],
              large:  ['500x500#',   :jpg],
              huge:   ['1000x1000#', :jpg],
              background: ['1440x916#', :jpg] },
            convert_options: {
              hires:   '-strip -depth 8 -quality 90',
              midres:   '-strip -depth 8 -quality 90',
              lores:   '-strip -depth 8 -quality 90',
              tiny:   '-flatten -strip -depth 8 -quality 90',
              small:  '-flatten -strip -depth 8 -quality 90',
              medium: '-flatten -strip -depth 8 -quality 90',
              large:  '-flatten -strip -depth 8 -quality 90',
              huge:   '-flatten -strip -depth 8 -quality 90',
              background:   '-flatten -strip -depth 8 -quality 90' }

    validates_attachment_content_type :<%= field.split(':').first %>, :content_type => /^post\/.*$/
  <%- end %>
<%- end %>

    private



      def set_default_publish_date
        self.publish_date = Time.now.to_s(:na) if publish_date.nil?
      end

      def prepare_slug
        self.slug = title.create_id if slug.blank?
        self.slug = unique_slug slug
      end

      def unique_slug(slug)
        if <%= blog_name.underscore %>.<%= post_name.tableize %>.any? { |p| p != self && p.slug == slug }
          # duplicate slug, need to make it unique
          # if -\d+$ matches increment end number, otherwise add a -1
          if slug =~ /-(\d+)$/
            slug.sub!(/\d+$/, ($~[1].to_i + 1).to_s)
          else
            slug = slug + '-1'
          end

          # try again
          return unique_slug slug
        end
        slug
      end
  end
end




