
_paths =
  links: ''
  pages: ''

pxcv.addHelpers


  frame<%= blog_name %>Path: (id) -> @framePagePath(id).replace(/pages/, '<%= blog_name.tableize %>')

  frame<%= blog_name %><%= post_name.pluralize %>Path: (id) -> @frame<%= blog_name %>Path(id) + '/<%= post_name.tableize %>'

  frame<%= blog_name %><%= post_name %>Path: (blogId, postId) ->
    @frame<%= blog_name %><%= post_name.pluralize %>Path(blogId) + "/#{postId}"

  newFrame<%= blog_name %><%= post_name %>Path: (blogId, postId) ->
    @frame<%= blog_name %><%= post_name %>Path(blogId, postId) + '/new'

  editFrame<%= blog_name %><%= post_name %>Path: (blogId, postId) ->
    @frame<%= blog_name %><%= post_name %>Path(blogId, postId) + '/edit'




