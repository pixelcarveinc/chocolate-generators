module Frame
  class <%= blog_name.pluralize %>Controller < ApplicationController

    include FrontendRenderable
    load_and_authorize_resource except: :show, class: Frame::<%= blog_name %>
    before_action :set_<%= blog_name.underscore %>, :set_<%= post_name.underscore %>, only: [:show]
    add_breadcrumb 'pages', :links_path
    add_breadcrumb 'edit', :edit_<%= blog_name.underscore %>_path, only: :edit
    decorates_assigned :<%= blog_name.underscore %>

    def index
      @<%= blog_name.tableize %> = Frame::<%= blog_name %>.all
    end

    def show

    end

    
    def edit
    end

    def update
      respond_to do |format|
        if @<%= blog_name.underscore %>.update(<%= blog_name.underscore %>_params)
          format.html { redirect_to links_path, notice: '<%= blog_name %> was successfully updated.' }
          format.json { render :show, status: :ok, location: @<%= blog_name.underscore %> }
        else
          format.html { render :edit }
          format.json { render json: @<%= blog_name.underscore %>.errors, status: :unprocessable_entity }
        end
      end
    end

    private

      def set_<%= blog_name.underscore %>
        @<%= blog_name.underscore %> = @page
      end

      def set_<%= post_name.underscore %>
        @<%= post_name.tableize %> = @<%= blog_name.underscore %>.<%= post_name.tableize %>.where(published: true).order(publish_date: :desc)
      end

      
      def <%= blog_name.underscore %>_params
        params.require(:<%= blog_name.underscore %>).permit(<%= @blog_fields.join(",") %>)
      end
  end
end
