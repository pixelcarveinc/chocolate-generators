module Frame
  class <%= blog_name %>Decorator < PageDecorator
    delegate_all

    decorates_association :<%= post_name.tableize %>

    def <%= post_name.tableize %>_path(options = {})
      h.<%= blog_name.underscore %>_<%= post_name.tableize %>_path object, options
    end

    def new_<%= post_name.underscore %>_path(options = {})
      h.new_<%= blog_name.underscore %>_<%= post_name.underscore %>_path object, options
    end

  end
end
