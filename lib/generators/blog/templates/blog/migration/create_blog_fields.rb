class CreateFrame<%= blog_name.pluralize %> < ActiveRecord::Migration
  def change
    

<%- @blog_attributes.each do |field| %>

  <%- if field.split(':').last == 'post' %>
    add_attachment :frame_pages, :<%= field.split(':').first %>
  <%- else %>
    add_column :frame_pages, :<%= field.split(':').first %>, :<%= field.split(':').last %>
  <%- end %>
<%- end %>
  end
end