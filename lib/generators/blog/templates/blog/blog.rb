module Frame
  class <%= blog_name %> < Page
    has_many :<%= post_name.tableize %>, inverse_of: :<%= blog_name.underscore %>, dependent: :destroy
    accepts_nested_attributes_for :<%= post_name.tableize %>, allow_destroy: true

<%- @blog_attributes.each do |field| %>
  <%- if field.split(':').last == 'post' %>
    has_attached_file :<%= field.split(':').first %>,
            styles: {
              hires:  ['1200x>',      :jpg],
              midres:  ['768x>',      :jpg],
              lores:  ['550x>',      :jpg],
              tiny:   ['40x40#',     :jpg],
              small:  ['100x>',      :jpg],
              medium: ['250x250#',   :jpg],
              large:  ['500x500#',   :jpg],
              huge:   ['1000x1000#', :jpg],
              background: ['1440x916#', :jpg] },
            convert_options: {
              hires:   '-strip -depth 8 -quality 90',
              midres:   '-strip -depth 8 -quality 90',
              lores:   '-strip -depth 8 -quality 90',
              tiny:   '-flatten -strip -depth 8 -quality 90',
              small:  '-flatten -strip -depth 8 -quality 90',
              medium: '-flatten -strip -depth 8 -quality 90',
              large:  '-flatten -strip -depth 8 -quality 90',
              huge:   '-flatten -strip -depth 8 -quality 90',
              background:   '-flatten -strip -depth 8 -quality 90' }

    validates_attachment_content_type :<%= field.split(':').first %>, :content_type => /^post\/.*$/
  <%- end %>
<%- end %>
  end
end
