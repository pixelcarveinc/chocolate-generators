module Frame
  class <%= blog_name %>Routable < Routable

    def content_routes(mapper)

      mapper.link.page.<%= post_name.tableize %>.where(published: true).each do |<%= post_name.underscore %>|
        mapper.get <%= post_name.underscore %>.slug, {
          controller: Frame::<%= post_name.pluralize %>Controller,
          defaults: {link_id: mapper.link.id, <%= post_name.underscore %>_id: <%= post_name.underscore %>.id}
        }
      end
    end

  end
end
