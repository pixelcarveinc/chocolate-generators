require 'active_record'
require 'rails/generators/actions/create_migration'
require 'rails/generators'
require 'rails/generators/migration'
require "rake"

ChocolateCMS::Application.load_tasks

class BlogGenerator < Rails::Generators::Base#NamedBase
  extend ActiveSupport::Concern
  include Rails::Generators::Migration
  #include ActiveRecord::Generators::Base

  source_root File.expand_path('../templates', __FILE__)

  argument :blog_name, type: :string, :required => true, banner:'Blog Type name'
  argument :post_name, type: :string, :required => true, banner:'Post Type name'
  argument :attributes, type: :array, default: [], banner: "blog_field:type blog_field:type ^ post_field:type post_field:type"


  def create_blog_essential_files
    blog_default_fields = [:type, :title, :body, :meta_description, :head_title, :published]
    post_default_fields = [:type, :category, :title, :body, :publish_date, :published]

    @blog_attributes = []
    @post_attributes = []

    

    #View USAGE file for syntax
    if attributes.index('^') == 0 #Post only fields
      @post_attributes = attributes[attributes.index('^')+1..-1]

      
    elsif attributes.index('^') == nil #Blog only fields

      @blog_attributes = attributes

  
    else #Fields for both types
      @blog_attributes = attributes[0..attributes.index('^')-1]
      @post_attributes = attributes[attributes.index('^')+1..-1]

      
    end

    @blog_fields = (blog_default_fields + @blog_attributes.map { |field| field.split(':').first}).map { |field| ':' + field.to_s } if @blog_attributes
    @post_fields = (post_default_fields + @post_attributes.map { |field| field.split(':').first}).map { |field| ':' + field.to_s } if @post_attributes
     

    


    template "blog/blog.rb", "app/models/frame/#{blog_name.underscore}.rb"
    template "blog/blogs_controller.rb", "app/controllers/frame/#{blog_name.tableize}_controller.rb"
    template "blog/blog_decorator.rb", "app/decorators/frame/#{blog_name.underscore}_decorator.rb"


  end

  def create_post_essential_files
    #default_fields = [:type, :title, :body, :meta_description, :head_title, :published]

    #@fields = (default_fields + attributes.map { |field| field.split(':').first}).map { |field| ':' + field.to_s }

    template "post/post.rb", "app/models/frame/#{post_name.underscore}.rb"
    template "post/posts_controller.rb", "app/controllers/frame/#{post_name.tableize}_controller.rb"
    template "post/post_decorator.rb", "app/decorators/frame/#{post_name.underscore}_decorator.rb"

  end

  def append_config_routes

    inject_into_file 'config/routes.rb', after: '# be edited in the CMS.' do
      "\n  resources :#{blog_name.tableize}, only: [:edit, :update] do\n    resources :#{post_name.tableize}, except: [:show], concerns: :multidestroyable\n  end"
    end

  end
  
  def append_application

    inject_into_file 'config/application.rb', after: '# Add custom page types' do
      "\n    config.frame.page_types << 'Frame::#{blog_name}'"
    end
  end

  def append_frame_router

    inject_into_file 'app/assets/javascripts/frontend/FrameRouter.js.coffee', after: '# Custom page types below' do
      "\n        when '#{blog_name}'\n          m[slug + '(/)'] = '#{blog_name.underscore.camelize(:lower)}Page'\n          m[slug + '/:#{post_name.underscore.camelize(:lower)}(/)'] = '#{post_name.underscore.camelize(:lower)}Page'"
    end
  end


  def create_javascript_class
    template "blog/PageType.js.coffee", "app/assets/javascripts/frontend/views/pages/#{blog_name}.js.coffee"
    template "post/PageType.js.coffee", "app/assets/javascripts/frontend/views/pages/#{post_name}.js.coffee"
  end

  def append_page_controller 

    inject_into_file 'app/assets/javascripts/frontend/PageController.js.coffee', after: '# Custom page types below' do
      "\n  #{blog_name.underscore.camelize(:lower)}Page: ->\n    @changePage '#{blog_name}'\n  #{post_name.underscore.camelize(:lower)}Page: ->\n    @changePage '#{post_name}'"
    end
  end

  def create_public_activity
 
    empty_directory "app/views/public_activity/frame_#{blog_name.underscore}"

    template "blog/public_activity/_create.haml", "app/views/public_activity/frame_#{blog_name.underscore}/_create.haml"
    template "blog/public_activity/_destroy.haml", "app/views/public_activity/frame_#{blog_name.underscore}/_destroy.haml"
    template "blog/public_activity/_update.haml", "app/views/public_activity/frame_#{blog_name.underscore}/_update.haml"
    
    empty_directory "app/views/public_activity/frame_#{post_name.underscore}"

    template "post/public_activity/_create.haml", "app/views/public_activity/frame_#{post_name.underscore}/_create.haml"
    template "post/public_activity/_destroy.haml", "app/views/public_activity/frame_#{post_name.underscore}/_destroy.haml"
    template "post/public_activity/_update.haml", "app/views/public_activity/frame_#{post_name.underscore}/_update.haml"

  end


  def create_views
    empty_directory "app/views/frame/#{blog_name.tableize}"
    template "blog/view/edit.html.haml", "app/views/frame/#{blog_name.tableize}/edit.html.haml"
    template "blog/view/show.html.haml", "app/views/frame/#{blog_name.tableize}/show.html.haml"

    empty_directory "app/views/frame/#{post_name.tableize}"
    template "post/view/edit.html.haml", "app/views/frame/#{post_name.tableize}/edit.html.haml"
    template "post/view/show.html.haml", "app/views/frame/#{post_name.tableize}/show.html.haml"
    template "post/view/_form.html.haml", "app/views/frame/#{post_name.tableize}/_form.html.haml"
    template "post/view/index.html.haml", "app/views/frame/#{post_name.tableize}/index.html.haml"
    template "post/view/new.html.haml", "app/views/frame/#{post_name.tableize}/new.html.haml"
  end

  def create_routables
 
    template "blog/routables/blog_routable.rb", "app/routables/frame/#{blog_name.underscore}_routable.rb"
  end

  def create_post_migration
    # if @behavior == :invoke
    #   migration_template "post/migration/create_post_attributes.rb", "db/migrate/create_frame_#{post_name.tableize}.rb"
    #   rake "db:migrate"
    if @behavior != :invoke && self.class.migration_exists?("db/migrate","create_frame_#{post_name.tableize}")
      
      version = self.class.migration_exists?("db/migrate","create_frame_#{post_name.tableize}").match(/[0-9]+/)
      
      #pry
      ENV["VERSION"] = version[0]

      Rake::Task["db:migrate:down"].invoke

      #FileUtils.rm("db/migrate/#{version}_create_frame_#{post_name.tableize}.rb")
    end
    #the following will create or delete deppending on @behavior
    migration_template "post/migration/create_post_fields.rb", "db/migrate/create_frame_#{post_name.tableize}.rb"
    
    #for some reason, the bottom line does not reverse migration, as a solution using db:migrate:down above
    rake "db:migrate"
  end

  def create_blog_migration
    if @behavior != :invoke && self.class.migration_exists?("db/migrate","create_frame_#{blog_name.tableize}")
      
      version = self.class.migration_exists?("db/migrate","create_frame_#{blog_name.tableize}").match(/[0-9]+/)
      

      #pry
      ENV["VERSION"] = version[0]
      Rake::Task["db:migrate:down"].reenable
      Rake::Task["db:migrate:down"].invoke

      #FileUtils.rm("db/migrate/#{version}_create_frame_#{post_name.tableize}.rb")
    end


    migration_template "blog/migration/create_blog_fields.rb", "db/migrate/create_frame_#{blog_name.tableize}.rb"

    rake "db:migrate"
  end



  def create_backend_routes
    empty_directory "app/assets/javascripts/backend/helpers"
    empty_directory "app/assets/javascripts/backend/models"
    empty_directory "app/assets/javascripts/backend/views"

    template "post/backend/model/Post.js.coffee", "app/assets/javascripts/backend/models/#{post_name}.js.coffee"
    template "post/backend/helper/paths.js.coffee", "app/assets/javascripts/backend/helpers/#{blog_name.underscore}_#{post_name.underscore}_paths.js.coffee"

  end


  private
  def self.next_migration_number(dirname) #:nodoc:

    next_num = current_migration_number(dirname) + 1
    

    ::ActiveRecord::Migration.next_migration_number(next_num)
  end
end
